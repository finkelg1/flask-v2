import unittest
from app import app

class FlaskTestCase(unittest.TestCase):
    def setUp(self):
        app.testing = True
        self.app = app.test_client()
        
    def test_home(self):
        response = self.app.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Guess a number between 0 and 9", response.data)

    def test_guess_too_high(self):
        response = self.app.get('/10')  # assuming random_number is between 0 and 9
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Too high, try again!", response.data)

    def test_guess_too_low(self):
        response = self.app.get('/-1')  # assuming random_number is between 0 and 9
        self.assertEqual(response.status_code, 200)
        self.assertIn(b"Too low, try again!", response.data)

    def test_guess_correct(self):
        # Here we need to mock random.randint to return a known value, e.g., 5
        with unittest.mock.patch('random.randint', return_value=5):
            response = self.app.get('/5')
            self.assertEqual(response.status_code, 200)
            self.assertIn(b"You found me!", response.data)

if __name__ == '__main__':
    unittest.main()
